<?php

namespace ExeQue;
/**
 * HaveIBeenPwned Breach Model
 *
 * Properties have been converted to lower_case naming
 *
 * @link https://haveibeenpwned.com/API/v2#BreachModel
 */
class HIBP_Breach {
    public $title;
    public $name;
    public $domain;
    public $breach_date;
    public $added_date;
    public $modified_date;
    public $pwn_count;
    public $description;
    public $data_classes;
    public $is_verified;
    public $is_fabricated;
    public $is_sensitive;
    public $is_active;
    public $is_retired;
    public $is_spam_list;
    public $logo_type;

    public function __construct($breach) {
        foreach ($breach as $key => $value) {
            $key = NCConvert::parse($key, NCConvert::PASCAL_CASE, NCConvert::LOWER_CASE);
            if (property_exists(get_class(), $key)) $this->{$key} = $value;
        }
    }

    public static function parseFromResponseData($response_object) {
        if ($response_object) {
            $breaches = [];
            foreach ($response_object as $breach) {
                $breaches[] = new HIBP_Breach($breach);
            }

            return $breaches;
        }

        return false;
    }
}